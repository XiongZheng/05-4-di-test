package com.twuc.webApp.web;

import com.twuc.webApp.service.GameLevelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MazeControllerTest2 {

    @Autowired
    private MockMvc mockMvc;

//    @TestConfiguration
//    static class Config{
//        @Bean
//        @Primary
//        public GameLevelService mockGameLevelService() throws IOException {
//            GameLevelService mock = mock(GameLevelService.class);
//            when(mock.renderMaze(10,10,"color")).thenReturn(new byte[]{1,2,3});
//            return mock;
//        }
//    }

    @MockBean
    private GameLevelService gameLevelService;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void should_return_bytes_123() throws Exception {
        when(gameLevelService.renderMaze(10, 10, "color")).thenReturn(new byte[]{1, 2, 3});
        mockMvc.perform(get("/buffered-mazes/color"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(new byte[]{1, 2, 3}));
    }
}
