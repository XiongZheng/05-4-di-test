package com.twuc.webApp.web;

import com.twuc.webApp.service.GameLevelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;
import java.io.OutputStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
public class MazeControllerTest {

    private MazeController mazeController;

    @Mock
    private GameLevelService gameLevelService;

    @BeforeEach
    void setUp() {
        initMocks(this);
        mazeController = new MazeController(gameLevelService);
    }

    @Test
    void should_return_correct_bytes_when_get_maze() throws IOException {
        //Arrange
        byte[] expected = new byte[]{1, 2, 3};
        when(gameLevelService.renderMaze(10, 10, "color")).thenReturn(expected);

        //Act
        ResponseEntity<byte[]> responseEntity = mazeController.getMaze(10, 10, "color");

        //Assert
        assertArrayEquals(expected, responseEntity.getBody());
    }

    @Test
    void should_test_getMaze2() throws IOException {

        MockHttpServletResponse response = new MockHttpServletResponse();

        mazeController.getMaze2(response, 10, 10, "color");

        verify(gameLevelService).renderMaze(response.getOutputStream(), 10, 10, "color");
        assertEquals(MediaType.IMAGE_PNG_VALUE, response.getContentType());
    }

    @Test
    void should_test_answering_call_to_void() throws IOException {

        MockHttpServletResponse response = new MockHttpServletResponse();

        mazeController.getMaze2(response, 10, 10, "color");

        doAnswer(invocation->{
            Object argument = invocation.getArgument(0);
            assertSame(response.getOutputStream(),argument);

            assertEquals(Integer.valueOf(10),invocation.getArgument(1));
            assertEquals(Integer.valueOf(10),invocation.getArgument(2));
            assertEquals("color",invocation.getArgument(3));
            assertEquals(MediaType.IMAGE_PNG_VALUE, response.getContentType());

            return null;
        }).when(gameLevelService).renderMaze(any(OutputStream.class),anyInt(),anyInt(),anyString());
    }
}
