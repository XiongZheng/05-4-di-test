package com.twuc.webApp.web;

import com.twuc.webApp.domain.mazeGenerator.AldousBroderMazeAlgorithm;
import com.twuc.webApp.domain.mazeGenerator.DijkstraSolvingAlgorithm;
import com.twuc.webApp.domain.mazeGenerator.Grid;
import com.twuc.webApp.domain.mazeRender.MazeWriter;
import com.twuc.webApp.service.GameLevelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
public class GameLeverServiceTest {
    @Autowired
    private AldousBroderMazeAlgorithm mazeAlgorithm;

    @Autowired
    private DijkstraSolvingAlgorithm solvingAlgorithm;

    private GameLevelService gameLevelService;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void should_test_practice() throws IOException {
        MazeWriter writer=mock(MazeWriter.class);
        when(writer.getName()).thenReturn("color");
        List<MazeWriter> list = new ArrayList<>();
        list.add(writer);
        gameLevelService = new GameLevelService(list, mazeAlgorithm, solvingAlgorithm);

        gameLevelService.renderMaze(10,10,"color");

        verify(writer).render(any(Grid.class),any(OutputStream.class));
    }
}
