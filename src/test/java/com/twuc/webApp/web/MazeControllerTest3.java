package com.twuc.webApp.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MazeControllerTest3 {

    @Autowired
    MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void should_return_img_and_get_8_byte() throws Exception {

        MvcResult mvcResult = mockMvc.perform(get("/buffered-mazes/color"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(MediaType.IMAGE_PNG_VALUE,mvcResult.getResponse().getContentType());

        byte[] expected = Arrays.copyOfRange(mvcResult.getResponse().getContentAsByteArray(), 0, 8);
        assertArrayEquals(new byte[]{(byte) 137, 80, 78, 71, 13, 10, 26, 10},expected);
    }
}
